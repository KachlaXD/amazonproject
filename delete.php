<?php
include_once "base/db.php";
if (!isset($_POST['id'], $_SESSION['user'])) {
    die("All fields are required");
}
include_once "base/core.php";

$core = new Core();
$core->deleteProductById($_POST['id']);
header("Location: list.php?id=" . $_POST['category_id']);
