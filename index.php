<?php
include_once "base/db.php";

require_once "base/Core.php";

$core = new Core();
$categories = $core->getCategories(4, 0);
$categories2ndLine = $core->getCategories(4, 4);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
          integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <title>Amazon.com-Shop Online</title>
</head>
<body>
<div class="sidebar">
    <div class="hdn-head">
        <h2>Hello, Sign in</h2>
    </div>
    <div class="hdn-content">
        <h3>Digital Content & Devices</h3>
        <ul>
            <div>
                <li>Amazon Music</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div>
                <li>Kindle E-Readers & Books</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div>
                <li>Appstore for Android</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
        </ul>
        <div class="line"></div>
    </div>
    <div class="hdn-content">
        <h3>Shop By Department</h3>
        <ul>
            <div onclick="">
                <li>Electronics</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div>
                <li>Computers</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div>
                <li>Smart Homes</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div>
                <li>Arts & Crafts</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
        </ul>
        <div class="line"></div>
    </div>
    <div class="hdn-content">
        <h3>Programs & Features</h3>
        <ul>
            <div>
                <li>Gift Cards & Mobile Recharges</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div>
                <li>Flight Tickets</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
            <div>
                <li>Clearance Store</li>
                <i class="fa-solid fa-angle-right"></i>
            </div>
        </ul>
        <div class="line"></div>
    </div>
</div>
<div class="triangle"><i class="fa-solid fa-triangle"></i></div>
<div class="hdn-sign">
    <div class="hdn-table">
        <div>
            <h3>Your Lists</h3>
            <ul>
                <li>Create a List</li>
                <li>Find a List & Registry</li>
                <li>Amazon Smile Charity Lists</li>
            </ul>
        </div>
        <div class="hdn-line"></div>
        <div>
            <h3>Your Account</h3>
            <ul>
                <li>Account</li>
                <li>Orders</li>
                <li>Recommendations</li>
                <li>Browsing History</li>
                <li>Watchlist</li>
                <li>Video Purchases</li>
                <li>Kindle Unlimited</li>
                <li>Content & Devices</li>
                <li>Subscribe & Save Items</li>
                <li>Membership</li>
                <li>Music Library</li>
            </ul>
        </div>
    </div>
</div>
<div class="black"></div>
<header>
    <div class="first">
        <div class="flex logo">
            <a href="#"><img src="images/logo.png" alt=""></a>
            <div class="map flex">
                <i class="fas fa-map-marker"></i>
                <div>
                    <span>Deliver to</span>
                    <span>Georgia</span>
                </div>
            </div>
        </div>
        <div class="flex input">
            <div>
                <span>All</span>
                <i class="fas fa-caret-down"></i>
            </div>
            <input type="text">
            <i class="fas fa-search"></i>
        </div>
        <div class="flex right">
            <div class="flex lang">
                <img src="images/usflag.jpg" alt="">
                <i class="fas fa-caret-down"></i>
            </div>
            <?php if (!isset($_SESSION['user'] )):?>
                <a class="sign" href="login/login.php">
                    <span>Hello, Customer</span>
                    <div class="flex ac">
                        <span>Log In</span>
                    </div>
                </a>
            <?php else:?>
                <a class="sign" href="login/logout.php">
                    <span>Hello, <?php echo $_SESSION['user'] ?></span>
                    <div class="flex ac">
                        <span>Log out</span>
                    </div>
                </a>
            <?php endif;?>

        </div>
    </div>
    <div class="second">
        <div class="second-1">
            <div>
                <i class="fas fa-bars"></i>
                <span>All</span>
            </div>
        </div>
        <div class="second-2">

            <ul>
                <li>Today's Deal</li>
                <li>Customer Service</li>
                <li>Registry</li>
                <li>Gift Cards</li>
                <li>Sell</li>
            </ul>
        </div>

    </div>
</header>
<section class="sec-1">
    <div class="container">
        <div class="container-1">
            <div class="product-comp">
                <?php foreach ($categories as $category) :?>
                <a class="box" href="list.php?id=<?php echo $category['id']?>">
                    <h3><?php echo $category['title'] ?></h3>
                    <div class="box-a">
                        <?php foreach ($category['products'] as $product) :?>
                        <div>
                            <img src="<?php echo $product['image'] ?>" alt="">
                            <span><?php echo $product['title']?></span>
                        </div>
                        <?php endforeach;?>
                    </div>
                </a>
                <?php endforeach;?>

            </div>
            <div class="product-comp">
                <?php foreach ($categories2ndLine as $category) :?>
                <a class="box" href="list.php?id=<?php echo $category['id']?>">
                    <h3><?php echo $category['title'] ?></h3>
                    <div class="box-a">
                        <?php foreach ($category['products'] as $product) :?>
                        <div>
                            <img src="<?php echo $product['image'] ?>" alt="">
                            <span><?php echo $product['title']?></span>
                        </div>
                        <?php endforeach;?>
                    </div>
                </a>
                <?php endforeach;?>

            </div>
        </div>
        <div class="slider">
            <div class="image-box">
                <div class="slide">
                    <img src="images/si1.jpg" alt="">
                </div>
                <div class="slide">
                    <img src="images/si2.jpg" alt="">
                </div>
                <div class="slide">
                    <img src="images/si3.jpg" alt="">
                </div>
                <div class="slide">
                    <img src="images/si4.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sec-2">
    <h3>Popular Products in Wireless Internationally</h3>
    <div class="slide-sec">
        <div class="l-btn"><i class="fa-solid fa-chevron-left"></i></div>
        <div class="r-btn"><i class="fa-solid fa-chevron-right"></i></div>
        <h3>Popular Products in Wireless Internationally</h3>
        <ul class="product-slide">
            <li><img src="images/pinter1.jpg" alt=""></li>
            <li><img src="images/pinter2.jpg" alt=""></li>
            <li><img src="images/pinter3.jpg" alt=""></li>
            <li><img src="images/pinter4.jpg" alt=""></li>
            <li><img src="images/pinter5.jpg" alt=""></li>
            <li><img src="images/pinter6.jpg" alt=""></li>
            <li><img src="images/pinter7.jpg" alt=""></li>
            <li><img src="images/pinter8.jpg" alt=""></li>
            <li><img src="images/pinter9.jpg" alt=""></li>
            <li><img src="images/pinter10.jpg" alt=""></li>
            <li><img src="images/pinter11.jpg" alt=""></li>
            <li><img src="images/pinter12.jpg" alt=""></li>
            <li><img src="images/pinter13.jpg" alt=""></li>
            <li><img src="images/pinter14.jpg" alt=""></li>
            <li><img src="images/pinter15.jpg" alt=""></li>
        </ul>
    </div>
</section>
<section class="sec-2">
    <!-- <h3>Popular Products in Wirelessly Internationally</h3> -->
    <div class="slide-sec">
        <div class="l-btn btn-1b"><i class="fa-solid fa-chevron-left"></i></div>
        <div class="r-btn btn-1a"><i class="fa-solid fa-chevron-right"></i></div>
        <h3>Popular Products in PC Internationally</h3>
        <ul class="product-slide product-slide-1">
            <li><img src="images/pc2.jpg" alt=""></li>
            <li><img src="images/pc3.jpg" alt=""></li>
            <li><img src="images/pc4.jpg" alt=""></li>
            <li><img src="images/pc5.jpg" alt=""></li>
            <li><img src="images/pc6.jpg" alt=""></li>
            <li><img src="images/pc7.jpg" alt=""></li>
            <li><img src="images/pc8.jpg" alt=""></li>
            <li><img src="images/pc9.jpg" alt=""></li>
            <li><img src="images/pc10.jpg" alt=""></li>
            <li><img src="images/pc11.jpg" alt=""></li>
            <li><img src="images/pc12.jpg" alt=""></li>
            <li><img src="images/pc13.jpg" alt=""></li>
            <li><img src="images/pc14.jpg" alt=""></li>
            <li><img src="images/pc15.jpg" alt=""></li>
        </ul>
    </div>
</section>

<section class="sec-2">
    <!-- <h3>Popular Products in Wirelessly Internationally</h3> -->
    <div class="slide-sec">
        <div class="l-btn btn-1c"><i class="fa-solid fa-chevron-left"></i></div>
        <div class="r-btn btn-1d"><i class="fa-solid fa-chevron-right"></i></div>
        <h3>Top Sellers in Books</h3>
        <ul class="product-slide product-slide-2">
            <li><img src="images/b1.jpg" alt=""></li>
            <li><img src="images/b2.jpg" alt=""></li>
            <li><img src="images/b3.jpg" alt=""></li>
            <li><img src="images/b4.jpg" alt=""></li>
            <li><img src="images/b5.jpg" alt=""></li>
            <li><img src="images/b6.jpg" alt=""></li>
            <li><img src="images/b7.jpg" alt=""></li>
            <li><img src="images/b8.jpg" alt=""></li>
            <li><img src="images/b9.jpg" alt=""></li>
            <li><img src="images/b10.jpg" alt=""></li>
        </ul>
    </div>
</section>
<section class="footer">
    <div class="backtop">
        <span>Back to Top</span>
    </div>
    <div class="detail">
        <div class="table">
            <div>
                <div class="t-head">Get to Know Us</div>
                <ul>
                    <li>Careers</li>
                    <li>Blogs</li>
                    <li>About Amazon</li>
                    <li>Investor Relations</li>
                    <li>Amazon Advices</li>
                    <li>Amazon Science</li>
                </ul>
            </div>

            <div>
                <div class="t-head">Make Money with Us</div>
                <ul>
                    <li>Sell Products on Amazon</li>
                    <li>Sell on Amazon Business</li>
                    <li>Sell Apps on Amazon</li>
                    <li>Become an Affiliate</li>
                    <li>Advertise your Products</li>
                    <li>Host an Amazon Hub</li>
                </ul>
            </div>


            <div>
                <div class="t-head">Amazon Payment Products</div>
                <ul>
                    <li>Amazon Business Cards</li>
                    <li>Shop with Points</li>
                    <li>Reload your Balance</li>
                    <li>Amazon Currency Converter</li>
                </ul>
            </div>


            <div>
                <div class="t-head">Let Us Help You</div>
                <ul>
                    <li>Amazon and COVID-19</li>
                    <li>Your Account</li>
                    <li>Yoyr Orders</li>
                    <li>Shipping Rates and Policies</li>
                    <li>Returns and Replacements</li>
                    <li>Manage your Content & Devices</li>
                    <li>Amazon Assistant</li>
                    <li>Help</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="line"></div>
    <div class="copy">

    </div>
</section>
<script src="app.js"></script>
</body>
</html>
