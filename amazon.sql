-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2024 at 12:07 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amazon`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`) VALUES
(1, 'Gaming'),
(2, 'Kids?'),
(3, 'Black Kids?'),
(4, 'Black Adults?'),
(5, 'Homies'),
(6, 'Sindisi'),
(7, 'Namusi'),
(8, 'Organs');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `category_id`, `title`, `image`, `description`, `price`) VALUES
(8, 1, 'Redragon K552', 'https://m.media-amazon.com/images/I/71cngLX2xuL._AC_SL1500_.jpg', 'Mechanical Gaming Keyboard 87 Key Rainbow LED Backlit Wired with Anti-Dust Proof Switches for Windows PC (Black Keyboard, Red Switches)', 60),
(9, 1, 'Logitech G502 HERO', 'https://m.media-amazon.com/images/I/61mpMH5TzkL._AC_SL1500_.jpg', 'High Performance Wired Gaming Mouse, HERO 25K Sensor, 25,600 DPI, RGB, Adjustable Weights, 11 Programmable Buttons, On-Board Memory, PC / Mac', 43),
(10, 1, 'ASUS ROG Hone', 'https://m.media-amazon.com/images/I/81MpNg1aLLL._AC_SL1500_.jpg', '508 X 420 x 3 mm, Large Size, Soft, Hybrid Cloth Material, Non-Slip Rubber Base, Esports & FPS Gaming, Black', 30),
(11, 1, 'SteelSeries Arctis Nova 1P', 'https://m.media-amazon.com/images/I/711IoMiPz4L._AC_SL1500_.jpg', 'Multi-System Gaming Headset — Hi-Fi Drivers — 360° Spatial Audio — Comfort Design — Durable — Lightweight — Noise-Cancelling Mic — PS5/PS4, PC, Xbox, Switch - White', 90),
(12, 2, 'Giorgi Chichilidze', 'https://media.licdn.com/dms/image/D4D03AQEs8SxboO1NAQ/profile-displayphoto-shrink_800_800/0/1684920489584?e=2147483647&v=beta&t=34wT2z2haihozCEPvUfQlBYFokJCHXrw5Dk0scEi6GA', 'ძალიან მსუქანია :(', 1),
(13, 2, 'Nikoloz Gvenetadze', 'https://i.mycdn.me/i?r=BDHElZJBPNKGuFyY-akIDfgncuz9szHywpgWwd4q53XVnwedQFkRaNiQraEWXOrOK3Q', 'Acoustic', 3),
(14, 2, 'Diana Mikeladze', 'https://i.mycdn.me/i?r=BDHElZJBPNKGuFyY-akIDfgn8PMhg7ngofaDgi9l1zD3gzrpRL2aeWtAMyWtVFgNx38', 'ქალმა უნდა იცოდეს სამი სიტყვა: მობრძანდით, დაბრძანდით, მიირთვით.', 4),
(15, 2, 'Vaniko Geladze', 'https://scontent.ftbs5-3.fna.fbcdn.net/v/t1.6435-9/50908565_2163792873950340_3076400527693578240_n.jpg?_nc_cat=105&ccb=1-7&_nc_sid=dd63ad&_nc_ohc=iNlFvQixiokAX8owdjN&_nc_ht=scontent.ftbs5-3.fna&oh=00_AfDUa6P2-cVjAy7XYuTOLh7JHOW5B7c8JOqog-8w0PqwKg&oe=65CE474E', 'გორელია', 0),
(16, 1, 'Logitech G502 HERO', 'https://m.media-amazon.com/images/I/61mpMH5TzkL._AC_SL1500_.jpg', 'High Performance Wired Gaming Mouse, HERO 25K Sensor, 25,600 DPI, RGB, Adjustable Weights, 11 Programmable Buttons, On-Board Memory, PC / Mac', 43),
(17, 3, 'Noah', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRWMPGD0AFOqrQqkMhiuu3ruX7mMC6UquYITA&usqp=CAU', 'Great for Cotton fields. ', 1),
(18, 3, 'Ethan', 'https://www.parents.com/thmb/J-4cajxXcd13i0rzWYSshli2LmA=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/IMG_0856-1-copy-674b17d762e446e699154bbae3e9779f.jpg', 'Great for Cotton fields. ', 5),
(19, 3, 'Cameron', 'https://images.unsplash.com/photo-1614710791641-4cd7a5c855f2?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8YmxhY2slMjBraWR8ZW58MHx8MHx8fDA%3D', 'Great for Cotton fields. ', 4),
(20, 3, 'Amir', 'https://img.freepik.com/free-photo/people-childhood-fun-leisure-lifestyle-concept-cute-adorable-afro-american-little-boy-casual-clothes-standing-with-turning-head-having-joyful-happy-facial-expression_343059-4529.jpg?size=626&ext=jpg&ga=GA1.1.1412446893.1704585600&semt=ais', 'Great for Cotton fields. ', 8),
(21, 4, 'Blackman 1', 'https://static2.bigstockphoto.com/3/9/2/large2/293545027.jpg', 'Does What u tell em to do', 100),
(22, 4, 'Blackman 2', 'https://thumbs.dreamstime.com/b/screaming-african-american-man-holding-large-heavy-chains-portrait-screaming-bare-chested-african-american-man-holding-very-197125640.jpg', 'Does What u tell em to do', 101),
(24, 4, 'Blackman 3', 'https://media.cnn.com/api/v1/images/stellar/prod/150803112112-levar-burton-kunta-kinteh.jpg?q=w_700,h_933,x_0,y_0,c_fill/h_618', 'Does What u tell em to do', 102),
(25, 4, 'Blackman 4', 'https://www.businessinsider.in/thumb/msid-30647077,width-700,height-525,imgsize-101641/many-people-fall-into-slavery-when-they-incur-debt-that-they-cannot-pay-back-then-the-money-lender-charges-them-astronomical-interest-rates-and-forces-them-to-work-.jpg', 'Does What u tell em to do', 103),
(26, 5, 'QiQo', 'https://scontent.ftbs5-2.fna.fbcdn.net/v/t1.6435-9/38011848_2645664832126540_4934738739579584512_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=dd63ad&_nc_ohc=0UAT-Ls7vp8AX8_7knG&_nc_ht=scontent.ftbs5-2.fna&oh=00_AfAGHLGUD9UPb8er6s2MoTF8P46-Oi7bx9sCS4gpPKsZhw&oe=65CE7267', 'Is short, Stuck in Gold4', 9999),
(27, 5, 'Dachi', 'https://scontent.ftbs5-3.fna.fbcdn.net/v/t1.6435-9/91436210_253538712479749_4944286404633952256_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=7f8c78&_nc_ohc=XXGYkpB0mrQAX-wGgSZ&_nc_ht=scontent.ftbs5-3.fna&oh=00_AfAz0WflfI7JdlgWP9yvWU8BgTCgeXAqWBtrslL5PnMhIw&oe=65CE73AE', 'Had a car', 99999),
(28, 5, 'Keka', 'https://i.mycdn.me/i?r=BDHElZJBPNKGuFyY-akIDfgnlWnulqQqpXGIgf5FM3xpmbX3mLVLeibe7sTIz8a5oI4', 'we go jim', 10000),
(29, 5, 'Asla', 'https://scontent.ftbs5-2.fna.fbcdn.net/v/t1.6435-9/34387945_1677844622334558_4208489817417187328_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=dd63ad&_nc_ohc=ByHGCFrudY0AX_peDvB&_nc_ht=scontent.ftbs5-2.fna&oh=00_AfDfCDCxUssI4KHr_Xy1u2WYaczTbDwbmSPTp_-1AAKy5Q&oe=65CE6086', 'OG', 4),
(30, 8, 'Heart', 'https://dvl2h13awlxkt.cloudfront.net/assets/general-images/Knowledge/_800x800_crop_center-center_82_none/heart-anatomy.png?mtime=1675729924', 'bum-bum', 12),
(31, 8, 'Lung', 'https://www.lifespan.io/wp-content/uploads/2020/11/lung-diagram.jpg', 'Need it to breath', 25),
(32, 8, 'Liver', 'https://badgut.org/wp-content/uploads/Image-Content-liver.png', 'Filters everything', 40),
(33, 8, 'Eye', 'https://www.nvisioncenters.com/wp-content/uploads/close-up-brown-eye.jpg', 'idk u see objects withit', 2000);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'giorgi@gmail.com', 'giorgi123'),
(2, 'beso@gmail.com', 'beso123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
