<?php
include_once "base/db.php";
if (!isset($_SESSION['user'])) {
    header("Location: login/login.php");
}
include_once "base/core.php";
$core = new Core();
?>
<html>
<head></head>
<body>
    <form action="store.php" method="POST">
        <input type="text" name="name" placeholder="Name">
        <input type="text" name="price" placeholder="Price">
        <input type="text" name="description" placeholder="Description">
        <select name="category_id">
            <?php foreach ($core->getCategories(100) as $category) { ?>
                <option value="<?php echo $category['id'] ?>"><?php echo $category['title'] ?></option>
            <?php } ?>
        </select>
        <button name="submit">Save</button>

    </form>
</body>
</html>
