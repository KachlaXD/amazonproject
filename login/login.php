<?php
    include "../base/db.php";

    $emailError = "";
    $passwordError = "";
    $error = "";
    $conn = new Db();
    if(isset($_POST['submit'])){
        $email = $_POST['e-mail'];
        $password = $_POST['password'];
        $query = "SELECT * FROM users WHERE email = '$email' AND password='$password'";
        $admin_result = $conn->connection->query($query);
        if (mysqli_num_rows($admin_result) == 1) {
            $_SESSION['user'] = $email;
            header("Location: ../index.php");
        }
        else{
            $error = "Invalid email or password!";
        }
    }
    elseif(empty($email)){
        $emailError = "Email is required!";

    }
    elseif(empty($password)){
        $passwordError = "Password is required!";
    }
    else{
        $error = "Invalid email or password!";
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../style/signin.css">
</head>
<body>
    <img src="../images/amazon-logo.png" alt="">
    <form action="" method="post" name="logging">
        <p class="SignInTxt">
            Sign in
        </p>
        <div >
            <input type="email" name="e-mail" placeholder="E-Mail">
            <?php
                echo $emailError;
            ?>
        </div>
        <div>
            <input type="password" name="password" placeholder="Password">
            <?php
                echo $passwordError;
            ?>
        </div>

        <button class="button1" name="submit">Log In</button>
        <div class="createAccount">
            <h2><span>New to Amazon?</span></h2>
            <button class="button2">Create your Amazon account</button>
        </div>
        <small>
            <?php
                echo $error;
            ?>
        </small>
    </form>
    <hr>

    <div class="extra">
        <p class="links"><a href="#" id="first">Conditions of Use</a>
        <a href="#">Notice of Use</a>
        <a href="#">Help</a></p>
        <p class="links" id="special">
            © 1996-2016, Amazon.com, Inc. or its affiliates
        </p>
    </div>


</body>
</html>
