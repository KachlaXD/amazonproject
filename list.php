<?php
if (!isset($_GET['id'])) {
    header("Location: index.php");
}
include_once "base/db.php";

require_once "base/Core.php";

$core = new Core();
$products = $core->getProductsByCategoryId($_GET['id']);
?>

<html>
<head>
    <link rel="stylesheet" href="style.css">
    <style>
    body {
      margin: 0;
      font-family: Arial, sans-serif;
    }

    table {
      width: 100%;
      border-collapse: collapse;
      margin-top: 20px;
    }

    th, td {
      padding: 8px;
      text-align: left;
    }

    th {
      background-color: black; 
      color: #ffffff;

    }
        
    </style>
</head>
<body>
<header>
    <div class="first">
        <div class="flex logo">
            <a href="index.php"><img src="images/logo.png" alt=""></a>
            <div class="map flex">
                <i class="fas fa-map-marker"></i>
                <div>
                    <span>Deliver to</span>
                    <span>Georgia</span>
                </div>
            </div>
        </div>
        <div class="flex input">
            <div>
                <span>All</span>
                <i class="fas fa-caret-down"></i>
            </div>
            <input type="text">
            <i class="fas fa-search"></i>
        </div>
        <div class="flex right">
            <div class="flex lang">
                <img src="images/usflag.jpg" alt="">
                <i class="fas fa-caret-down"></i>
            </div>
            <?php if (!isset($_SESSION['user'] )):?>
                <a class="sign" href="login/login.php">
                    <span>Hello, Customer</span>
                    <div class="flex ac">
                        <span>Log In</span>
                    </div>
                </a>
            <?php else:?>
                <a class="sign" href="login/logout.php">
                    <span>Hello, <?php echo $_SESSION['user'] ?></span>
                    <div class="flex ac">
                    </div>
                </a>
            <?php endif;?>

        </div>
    </div>
</header>
    <table>
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Description</th>
            <th>Category</th>
            <th>Actions</th>
        </tr>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td><?php echo $product['title'] ?></td>
                <td><?php echo $product['price'] . "$"?></td>
                <td><?php echo $product['description'] ?></td>
                <td><?php echo $product['category']['title'] ?></td>
                <td>
                    <?php if(isset($_SESSION['user'])):?>
                    <a href="edit.php?id=<?php echo $product['id'] ?>">Edit</a>
                    <form action="delete.php" method="POST">
                        <input type="hidden" name="id" value="<?php echo $product['id'] ?>">
                        <input type="hidden" name="category_id" value="<?php echo $product['category_id'] ?>">
                        <button name="submit">Delete</button>
                    </form>
                    <?php endif;?>
                </td>
            </tr>
        <?php } ?>
    </table>
    <a href="add.php">Add new product</a>
</body>
</html>
