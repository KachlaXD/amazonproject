<?php
require_once "db.php";
class Core extends Db
{

    public function __construct()
    {
        parent::__construct();
    }
    public function getCategories($total=4, $skip=0)
    {
        $result = $this->connection->query("SELECT * FROM category LIMIT $skip, $total");
        $categories = [];
        while ($row = $result->fetch_assoc()) {
            $row['products'] = $this->getProductsByCategoryId($row['id'], 4);
            $categories[] = $row;

        }
        return $categories;

    }

    public function getCategoryById($id)
    {
        $result = $this->connection->query("SELECT * FROM category WHERE id = $id");
        return $result->fetch_assoc();
    }
    public function getProductsByCategoryId($categoryId, $limit = 4, $start = 0)
    {
        $result = $this->connection->query("SELECT * FROM product WHERE category_id = $categoryId LIMIT $start, $limit");
        $products = [];
        while ($row = $result->fetch_assoc()) {
            $row['category'] = $this->getCategoryById($row['category_id']);
            $products[] = $row;
        }
        return $products;
    }

    public function deleteProductById($id)
    {
        $this->connection->query("DELETE FROM product WHERE id = $id");
    }

    public function getProductById($id)
    {
        $result = $this->connection->query("SELECT * FROM product WHERE id = $id");
        return $result->fetch_assoc();
    }

    public function updateProduct($id, $name, $price, $description, $category_id)
    {
        $this->connection->query("UPDATE product SET title = '$name', price = $price, description = '$description', category_id = $category_id WHERE id = $id");
    }

    public function addProduct($name, $price, $description, $category_id)
    {
        $this->connection->query("INSERT INTO product (title, price, description, category_id) VALUES ('$name', $price, '$description', $category_id)");
    }

}
