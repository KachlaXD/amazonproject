<?php
if (!isset(
    $_POST['id'],
    $_POST['name'],
    $_POST['price'],
    $_POST['description'],
    $_POST['category_id'],
    $_SESSION['user']
    )
) {
    die("All fields are required");
}
include_once "base/db.php";

include_once "base/core.php";

$core = new Core();
$core->updateProduct(
    $_POST['id'],
    $_POST['name'],
    $_POST['price'],
    $_POST['description'],
    $_POST['category_id']
);
header("Location: list.php?id=" . $_POST['category_id']);
