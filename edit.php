<?php
include_once "base/db.php";

if (!isset($_GET['id'], $_SESSION['user'])) {
    header("Location: index.php");
}

include_once "base/core.php";

$core = new Core();
$product = $core->getProductById($_GET['id']);
?>
<html>
<head></head>
<body>
    <form action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $product['id'] ?>">
        <input type="text" name="name" value="<?php echo $product['title'] ?>">
        <input type="text" name="price" value="<?php echo $product['price'] ?>">
        <input type="text" name="description" value="<?php echo $product['description'] ?>">
        <select name="category_id">
            <?php foreach ($core->getCategories(100) as $category) { ?>
                <option value="<?php echo $category['id'] ?>" <?php echo $category['id'] == $product['category_id'] ? 'selected' : '' ?>><?php echo $category['title'] ?></option>
            <?php } ?>
        </select>
        <button name="submit">Save</button>
    </form>
</body>
</html>
